public class Collision {
	
	Collidable first, second;
	
	public Collision(Collidable A, Collidable B) {
		first = A;
		second = B;
	}
	
	public Collidable getFirstItem() {
		return first;
	}
	
	public Collidable getSecondItem() {
		return second;
	}
}
	