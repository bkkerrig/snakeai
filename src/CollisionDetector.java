import java.util.LinkedList;

public class CollisionDetector {
		
	
		public CollisionDetector() {}

		public LinkedList<Collision> gatherCollisions(LinkedList<Collidable> collidables) {
			LinkedList<Collision> collisions = new LinkedList<Collision>();
			
			for(Collidable a : collidables) {
				
				for(Collidable b : collidables) {
					
					if(a == b)
						continue;
					
					if(a.isColliding(b)) {
						
						boolean passed = true;
						
						for(Collision c : collisions) {
							if(c.getFirstItem() == b && c.getSecondItem() == a)
								passed = false;
						}
						
						if(passed)
							collisions.add(new Collision(a, b));
					}
				}
			}
			
			return collisions;			
		}
	}