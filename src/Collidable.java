abstract public class Collidable extends Tangible {

	HitBox h;
	
	private class HitBox {
		int pos_x, pos_y, widthX, widthY;
		
		public HitBox(int x, int y, int wx, int wy) {
			pos_x = x;
			pos_y = y;
			widthX = wx;
			widthY = wy;
		}
		
		public int getX() {
			return pos_x;
		}
		
		public int getY() {
			return pos_y;
		}

		public int getWidthX() {
			// TODO Auto-generated method stub
			return widthX;
		}

		public int getWidthY() {
			// TODO Auto-generated method stub
			return widthY;
		}
		
	}
	
	public Collidable(int x, int y, int widx, int widy) {
		
		super(x, y);
		h = new HitBox(x, y, widx, widy);
	}
	
	public HitBox getHitBox() {
		return h;
	}
	
	public boolean isColliding(Collidable candidateCollidable) {
		HitBox a = h;
		HitBox b = candidateCollidable.getHitBox();
		//Check if A.X +- WIDTHS is  
		boolean colliding = false;
		
		int z = a.getX() + a.getWidthX();
		int w = a.getX();
		
		if(z <= (b.getX() + b.getWidthX()) && (z >= b.getX())) {
			colliding = true;
		}
		
		if(w <= (b.getX() + b.getWidthX()) && (w >= b.getX())) {
			colliding = true;
		}	
		
		
		if(!colliding)
			return false;
		
		z = a.getY() + a.getWidthY();
		w = a.getY();

		if(z <= (b.getY() + b.getWidthY()) && (z >= b.getY())) {
			colliding = true;
		} else if (w <= (b.getY() + b.getWidthY()) && (w >= b.getY())) {
			colliding = true;
		} else {
			colliding = false;
		}
		
		return colliding;
		
	}
	
	public int getWidthX() {
		return h.widthX;
	}

	public int getWidthY() {		// TODO Auto-generated method stub
		return h.widthY;
	}
	
}
