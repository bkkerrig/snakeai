import java.awt.Point;

public class Tangible {
	int pos_x;
	int pos_y;

	public Tangible(int x, int y) {
		pos_x = x;
		pos_y = y;
		
	}
	
	public int getX() {
		return pos_x;
	}
	
	public int getY() {
		return pos_y;
	}
	
	public int hashedCoordinates() {
        int hash = 17;
        hash = ((hash + pos_x) << 5) - (hash + pos_x);
        hash = ((hash + pos_y) << 5) - (hash + pos_y);
        return hash;
        				
	}
	
}
