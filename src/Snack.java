import java.awt.Color;
import java.awt.Graphics;

public class Snack extends Collidable implements Drawable {
	
	public Snack(int x, int y) {
		super(x, y, 10, 10);
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(pos_x, pos_y, 10, 10);
	}

}
