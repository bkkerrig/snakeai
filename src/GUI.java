import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;

import javax.swing.*;

public class GUI extends JFrame {
	
	Graphics g;
	GameInstance gameInstance;
	
	GUI(GameInstance i) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 800);
		
        Canvas canvas = new Canvas();
        canvas.setSize(800, 800);
		//setBackground(Color.BLACK);

        add(canvas);
        setVisible(true);

        g = canvas.getGraphics();
        gameInstance = i;
	}

	public void update(LinkedList<Drawable> drawables) {
		g.clearRect(0, 0, 800, 800);
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 800, 800);

		for(Drawable d : drawables) {
			d.draw(g);
		}

	}


}