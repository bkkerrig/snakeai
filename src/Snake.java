import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.*;

public class Snake implements Drawable{

	LinkedList<SnakeBit> bits;
	int deltaX;
	int deltaY;
	boolean grow;
	SnakeAI snakeAI;
	int size;
	int movePadding;
	Color color;
	
	public Snake(int x, int y, LinkedList<Snake> snakes, Color c) {
		bits = new LinkedList<>();
		SnakeBit head = new SnakeBit(x, y);
		bits.add(head);
		deltaX = 1;
		deltaY = 0;
		grow = false;
		snakeAI = new SnakeAI(this, snakes);
		size = 1;
		movePadding = 12;
		color = c;
	}
	
	public SnakeBit getHeadBit() {
		return bits.getLast();
	}

	public void setDeltaX(int d) {
		deltaX = d;
	}
	
	public void setDeltaY(int d) {
		deltaY = d;
	}
	
	public boolean validDirection(int dx, int dy) {
		if(deltaY == 1 && dy == -1)
			return false;
		if(deltaY == -1 && dy == 1)
			return false;
		if(deltaX == 1 && dx == -1)
			return false;
		if(deltaX == -1 && dx == 1)
			return false;
		
		return true;
		
	}
	
	
	private class Path {
		
		LinkedList<SnakeBit> bits;

		public Path(int x, int y) {

			bits = new LinkedList<SnakeBit>();
			bits.add(new SnakeBit(x, y));
		}
		
		public Path(SnakeBit b) {
			bits = new LinkedList<SnakeBit>();
			bits.add(b);
		}
		
		public Path(Path existingPath) {
			LinkedList<SnakeBit> oldBits = existingPath.getBits();
			bits = new LinkedList<SnakeBit>();
			for (SnakeBit b : oldBits) {
				bits.add(b);
			}
		}

		public SnakeBit getCurrentBit() {
			return bits.getLast();
		}
		
		public int getPathCost() {
			return bits.size();
		}
		
		public LinkedList<SnakeBit> getBits(){
			return bits;
		}
		
		public void addBit(SnakeBit s){
			bits.add(s);
		}
	}
	
	public class PathComparator implements Comparator<Path> {
	    @Override
	    public int compare(Path x, Path y)
	    {
	        // Assume neither string is null. Real code should
	        // probably be more robust
	        // You could also just return x.length() - y.length(),
	        // which would be more efficient.
	        if (x.getPathCost() > y.getPathCost())
	        {
	            return 1;
	        }
	        if (x.getPathCost() < y.getPathCost())
	        {
	            return -1;
	        }
	        return 0;
	    }
	}
	
	private class SnakeAI {
		
		Snake currentSnake;
		Snack currentSnack;

		LinkedList<Snake> currentSnakes;

		public SnakeAI(Snake s, LinkedList<Snake> snakes) {
			currentSnake = s;
			currentSnakes = snakes;
		}

		public Path calculatePath(GUI gui) {
			
			Comparator<Path> comparator = new PathComparator();
			PriorityQueue<Path> frontier = new PriorityQueue<Path>(100, comparator);


			HashMap<Integer, SnakeBit> explored = new HashMap<Integer, SnakeBit>();
			
			SnakeBit head = currentSnake.getSnakeBits().getLast();
			//System.out.printf("The current head is at (%d, %d)\n", head.getX(), head.getY());
			LinkedList<SnakeBit> otherBits = head.getAdjacentBits();
			//System.out.print(otherBits.size());
			for(SnakeBit b : otherBits ) {
				frontier.add(new Path(b));
			}
			
			for(SnakeBit b : currentSnake.getSnakeBits()) {
				explored.put(b.hashedCoordinates(), b);
			}
			

			while (!frontier.isEmpty()) {
				
				Path currentPath = frontier.remove();
				SnakeBit currentBit = currentPath.getCurrentBit();

				//add node to explored
				explored.put(currentBit.hashedCoordinates(), currentBit);
				//currentBit.draw(gui.g);
				//If we are colliding with the goal then we should use this path
				if(currentPath.getCurrentBit().isColliding(currentSnack)) {
					return currentPath;
				}
				boolean broken = false;
				for(Snake s : currentSnakes) {
					for(SnakeBit b : s.getSnakeBits()) {
						if(b != currentBit) {
							if(currentBit.isColliding(b)) {
								broken = true;
							}
						}
					}
				}

				if(broken)
					continue;
				
				for(SnakeBit b : currentBit.getAdjacentBits()) {
					int skip = new Random().nextInt(2);
					if(skip == 1)
						continue;
					Path existingInFrontier = null;
					
					for(Path p : frontier) {
						if(p.getCurrentBit().getX() == b.getX()) {
							if(p.getCurrentBit().getY() == b.getY()) {
								existingInFrontier = p;
							}
						}
					}
					
					Path newPath = new Path(currentPath);
					newPath.addBit(b);
					
					if(!explored.containsValue(b.hashedCoordinates()) && existingInFrontier == null) {
						
						frontier.add(newPath);
						
					} else if(existingInFrontier != null){
						if(newPath.getPathCost() < existingInFrontier.getPathCost()) {
							frontier.remove(existingInFrontier);
							frontier.add(newPath);
						}
					}
				}
			}
			
			return null;			
		}
				
	}

	public void aiMove(Snack currentSnack, GUI gui) {


		snakeAI.currentSnack = currentSnack;
		
		Path p = snakeAI.calculatePath(gui);
		
		if(p != null) {
			SnakeBit head = getHeadBit();
			SnakeBit nextMove = p.getBits().getFirst();

			for(int i = -1; i <= 1; i++) {
				for(int j = -1; j <= 1; j++) {
					if ((i == 0 && (j == -1 | j == 1)) |
						(j == 0 && (i == -1 | i == 1))) {
						int x = head.getX() + i * movePadding;
						int y = head.getY() + j * movePadding;
						
						if(x == nextMove.getX())
							if(y == nextMove.getY()) {
								setDeltaX(i);
								setDeltaY(j);
								//System.out.printf("Moving %d %d\n", i, j);
							}
					}
				}
			}
			
		} else {
			//System.out.println("There is no path");
		}
		
		
		move();
	}
	
	public void move() {

		SnakeBit head;

		if(!grow && bits.size() > 1) {
			bits.removeFirst();
		} else {
			grow = false;
			if(bits.size() < 5)
				size++;
		}
		
		head = bits.peekLast();
		
		SnakeBit newHead = new SnakeBit(head.getX() + deltaX * movePadding,
										head.getY() + deltaY * movePadding
										);
		bits.add(newHead);
		//System.out.printf("The ACTUAL head is at (%d, %d)\n", head.getX(), head.getY());
	}
	
	
	public class SnakeBit extends Collidable implements Drawable {
		int movePadding = 12;
		boolean fake = false;
		public SnakeBit(int x, int y) {
			super(x, y, 10, 10);
		}

		@Override 
		public void draw(Graphics g) {

		      g.drawRect(pos_x, pos_y, 10 /*TODO Change these to match the hitbox*/, 10);
		      g.drawOval(pos_x  + 5, pos_y + 5, 1, 1);
		}
		
		public LinkedList<SnakeBit> getAdjacentBits() {
			LinkedList<SnakeBit> adjacentBits = new LinkedList<SnakeBit>();
			
			for(int i = -1; i <= 1; i++) {
				for(int j = -1; j <= 1; j++) {
					if ((i == 0 && (j == -1 | j == 1)) |
						(j == 0 && (i == -1 | i == 1))) {
						
						SnakeBit d = new SnakeBit(getX() + i * movePadding, getY() + j * movePadding);
						d.fake = true;
						adjacentBits.add(d);
					}
				}
			}
			return adjacentBits;
			
		}
	
	}

	@Override
	public void draw(Graphics g) {

		for(SnakeBit bit : bits) {
			g.setColor(color);
			bit.draw(g);
		}
	}

	public LinkedList<SnakeBit> getSnakeBits() {
		// TODO Auto-generated method stub
		return bits;
	}

	public void setGrow(boolean g) {
		// TODO Auto-generated method stub
		grow = g;
	}
	
}
