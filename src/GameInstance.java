import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GameInstance {
	
	GUI gui;
	CollisionDetector collisionDetector;
	LinkedList<Snake> snakes;
	Snack currentSnack;
	Random random;
	
	/*Collision detection should be done with a hash map*/
	public GameInstance() {
		snakes = new LinkedList<>();
		random = new Random();
		createSnakes();
		currentSnack = generateSnack();
		collisionDetector = new CollisionDetector();
		gui = new GUI(this);
	}

	public int newPointInBounds() {
		return random.nextInt(300) + 250;
	}
	private void createSnakes() {
		snakes.clear();
		snakes.add(new Snake(newPointInBounds(), newPointInBounds(), snakes, Color.RED));
		snakes.add(new Snake(newPointInBounds(),newPointInBounds(), snakes, Color.YELLOW));
		snakes.add(new Snake(newPointInBounds(),newPointInBounds(), snakes, Color.CYAN));
		snakes.add(new Snake(newPointInBounds(),newPointInBounds(), snakes, Color.GREEN));
		snakes.add(new Snake(newPointInBounds(),newPointInBounds(), snakes, Color.ORANGE));
		snakes.add(new Snake(newPointInBounds(),newPointInBounds(), snakes, Color.PINK));


	}
	private LinkedList<Drawable> getDrawables() {
	
		LinkedList<Drawable> drawables = new LinkedList<Drawable>();
		
		for(Snake s : snakes) {
			drawables.add(s);
		}
		drawables.add(currentSnack);
		
		return drawables;
		
		
	}
	
	private LinkedList<Collidable> getCollidables() {
		
		LinkedList<Collidable> collidables = new LinkedList<Collidable>();
		
		/*Should the snake bits be private?*/
		for(Snake s : snakes) {
			for(Collidable snakeBit : s.getSnakeBits()) {
				collidables.add(snakeBit);
			}
		}

		collidables.add(currentSnack);

		return collidables;
		
		
	}
	
	public void update() {
	
		//snake.move();
		for(Snake s : snakes) {
			s.aiMove(currentSnack, gui);
		}

	}
	
	public static void sleep() {
		long interval = 100000000;
		long start, end;
		start = System.nanoTime();
		do {
			end = System.nanoTime();
		} while((end - start) < interval);
		
	}
	
	public void run() {
		int count = 0;
		
		while(true) {
			gui.update(getDrawables());
			sleep();
			LinkedList<Collision> collisions = collisionDetector.gatherCollisions(getCollidables());
			
			for(Collision c : collisions) {
				handleCollision(c);
			}

			gui.pack();
			if(count > 0) {
				update();
			}
			count++;
		}
	}
	
	private void handleCollision(Collision c) {
		if(c.getFirstItem() instanceof Snake.SnakeBit) {
			if(c.getSecondItem() instanceof Snack) {
				for(Snake s : snakes) {
					for(Snake.SnakeBit b : s.getSnakeBits()) {
						if(b == c.getFirstItem()) {
							s.setGrow(true);
						}
					}
				}
				currentSnack = generateSnack();
			}
			
			if(c.getSecondItem() instanceof Snake.SnakeBit) {
				restartGame();
			}

		}
		
	}
	
	private Snack generateSnack() {
		return new Snack(newPointInBounds(), newPointInBounds());
	}

	private void restartGame() {
		// TODO Auto-generated method stub
		createSnakes();
		currentSnack = generateSnack();
	}

	public static void main(String[] args) {
		
		GameInstance gameInstance = new GameInstance();
		gameInstance.run();

	}
	
}
